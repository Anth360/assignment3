
// DATA TO DOM 
//create a function that takes in a object as its parameter {name: "shane", comment: "this was great"}

var comment = {
    name: "Jack Deng",
    msg: "They BLEW the ROOF off at their last show, once everyone started figuring out they were going.This is still simply the greatest opening of a concert I have EVER witnessed."
}

var comments = [comment, comment, {name: 'Anthony', msg: "Great site"}]


commentLooper(comments);

//create function to loop through array of comments and invoke the function. 

function commentLooper(x) {

for (let i = 0; i < x.length; i++){
    presentComments(x[i])
    }

}

function presentComments(y) {

console.log('presentComments')

// var commentContainer = document.getElementById('commentContainer');
// var commentSection = document.createElement('div');

// commentContainer.appendChild(commentSection);
// var nameElement = document.createElement('p');

// nameElement.innerHTML = comment.name;
// commentSection.appendChild(nameElement);

// var messageElement= document.createElement('p');
// commentSection.appendChild(messageElement);

// msgElement.innerHTML = comment.msg;
// var postedComments = document.createElement('div');
// commentSection.appendChild(postedComments);

// put those 2 elements into another div, which acts as a wrapper
// put that element on the DOM

    // Creating Empty HTML Elements and storing them in unique variables. 
    var authorElement = document.createElement('h3');
    // Referencing CSS styling through Javascript 
    authorElement.className = 'user--name';
    // Repeating previous two steps for comments
    var commentElement = document.createElement('p');
    commentElement.className = 'user--comment'
    // Pulling author and comment "string" (values) out of object y and putting them in unique variables
    var currentAuthor = y.name; 
    var currentComment = y.msg;
    // Inject text from current object into innerHTML for each relevant tag
    // Taking newly created empty HTML elements and assigning them stored strings 
    authorElement.innerHTML = currentAuthor;
    commentElement.innerHTML = currentComment;
    // Creating a wrapper div, (an author H3, a p tag for comments) (javascript) using document.createElement 
    // Creating a new HTML div to hold filled-in author and comment HTML elemenets. 
    var commentWrapper = document.createElement('div');
    // Creating horizontal ruler to separate comment entries
    var ruler = document.createElement('hr');
    ruler.className = ('horizontal--ruler');
    
    // Appending H3 + Ptag into div wrapper (purely organizational)
    // Appending author h3 into "commentWrapper" HTML div. 
    commentWrapper.appendChild(ruler);
    commentWrapper.appendChild(authorElement);
    commentWrapper.appendChild(commentElement);
    // Store in a new variable, the target div (on dom you want to append to) 
    var testId = document.getElementById('testId');
    // Appending wrapper div to target div 
    testId.appendChild(commentWrapper);

}

// Create an event listener that detects button clicks, and triggers a function
// EVENT TRIGGER FUNCTION
// create a function that gets triggered by an event listener
    // use a query selector to store each input as a variable
    // Store each value from the inputs inside their own unique variables
    // Put these variables inside an object
    // Pass object to DATA TO DOM function

    var box = document.getElementById('submit-button'); 
    box.addEventListener('click', submitHandler);

    function submitHandler(event){
        event.preventDefault();

    //Use doc. getelementbyID to store each input of your form in a variable. 
    //Make another two variables, store the previous two element's values (element.value)

    //grab the name inpiut DOM object
    var commenterName = document.getElementById ('commenterName')
    //pull the value out of the DOM object using . value
    var commenterNameValue = commenterName.value;

    //same thing we did for above name but now for the comments section 
    var commenterOpinion = document.getElementById ('commenterOpinion')
    //pull the value out of the object using . value
    var commenterOpinionValue = commenterOpinion.value;

    //Use commentObjects variable and use the .push function to append newly
    //Submitted value to the end of the array 

    comments.push({author: commenterNameValue, comment: commenterOpinionValue})
    
    //Call the presentComments function and pass the latest entry to the commentObjects array as an agument
    //in order to post the new entry to the page
    presentComments(comments[comments.length-1]);

    }
   
